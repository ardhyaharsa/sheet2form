var uploaded;

var upload_container = document.getElementById('upload-container');
var uploader = new plupload.Uploader({
  runtimes : 'html5,flash,silverlight,html4',
  multi_selection : false,
  browse_button : 'btn-upload',
  container : upload_container,
  url : url(upload_container.getAttribute('dt-upload-url')),
  filters :
  {
    max_file_size : '10mb',
    mime_types: [
      {title : "Spreadsheet File", extensions : "xlsx"},
    ]
  },
  init:
  {
    PostInit: function()
    {
    },
    FilesAdded: function(up, files)
    {
      uploader.start();
    },
    UploadProgress: function(up, file)
    {
    },
    FileUploaded: function(up, file, info)
    {
      var response = JSON.parse(info.response);
      uploaded = response.file;
    },
    UploadComplete: function(up, files)
    {
      var data = {
        upload: uploaded
      };

      let newTab = window.open();
      newTab.location.href = upload_container.getAttribute('dt-setting-url') + '?file=' + window.btoa(JSON.stringify(data));
    },
    Error: function(up, err)
    {
      $.notify(err.message, 'error');
    }
  }
});
uploader.init();

$(document).ready(function()
{
  $(document).on('click', '.btn-download', function()
	{
    window.open(url($(this).attr('dt-url')));
	});
});
