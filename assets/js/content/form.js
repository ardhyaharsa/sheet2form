function get_form()
{
  var data = {
    form_id : window.location.href.split('/').pop()
  };

  var url = $('.form_list').attr('dt-url');
  $.get(url , data ,
    function(result)
    {
      $('.form_list').html(result);
    }
  );
}

get_form();
