$(document).ready(function()
{
  const background = new Pickr({
    el: '.background-color-picker .icon',
    useAsButton: true,
    disabled: false,
    comparison: true,
    default: background_color,
    defaultRepresentation: 'HEX',
    showAlways: false,
    parent: null,
    closeWithKey: 'Escape',
    position: 'middle',
    adjustableNumbers: true,
    components: {
        preview: false,
        opacity: true,
        hue: true,
        interaction: {
            hex: true,
            rgba: true,
            hsla: false,
            hsva: false,
            cmyk: false,
            input: true,
            clear: false,
            save: true
        },
    },
    strings: {
       save: 'Save',
       clear: 'Clear'
    },
    onSave(hsva, instance) {
      background_color = hsva.toHEX().toString().substring(1);

      var color = tinycolor(hsva.toHEX().toString());
      var color_2nd = color.isLight() ? '#4a4a4a' : '#fff';

      $('.container.form-container.forms').css('background' , hsva.toHEX());
      $('.container.form-container.forms .background-color-picker').css('color' , color_2nd);
    }
  });

  const header = new Pickr({
    el: '.header-color-picker .icon',
    useAsButton: true,
    disabled: false,
    comparison: true,
    default: header_color,
    defaultRepresentation: 'HEX',
    showAlways: false,
    parent: null,
    closeWithKey: 'Escape',
    position: 'middle',
    adjustableNumbers: true,
    components: {
        preview: false,
        opacity: true,
        hue: true,
        interaction: {
            hex: true,
            rgba: true,
            hsla: false,
            hsva: false,
            cmyk: false,
            input: true,
            clear: false,
            save: true
        },
    },
    strings: {
       save: 'Save',
       clear: 'Clear'
    },
    onSave(hsva, instance) {
      header_color = hsva.toHEX().toString().substring(1);

      var color = tinycolor(hsva.toHEX().toString());
      var color_2nd = color.isLight() ? '#4a4a4a' : '#fff';

      $('.container.form-container.forms .columns.header').css('background' , hsva.toHEX());
      $('.container.form-container.forms .columns.header .column').css('color' , color_2nd);
    }
  });

  $(document).on('change', '.font-family', function()
  {
    check_font();
  });

  $(document).on('keyup', '.button-name', function()
	{
    check_button();
	});

  $(document).on('keyup', '.complete-text', function()
	{
    check_complete();
	});

  $(document).on('click', '.btn-undo', function()
	{
    get_form();
	});

  $(document).on('click', '.btn-save', function()
	{
    var form = [];
    $('.columns.page .field').each(function( index )
    {
      var value = false;
      var this_ = $(this);
      var type = this_.attr('dt-type');

      if(type)
      {
        var question = this_.find('.label').html();
        var options = false;

        switch(type)
        {
          case 'check-boxes':
          case 'multiple-choice':
            options = [];
            var input_type = (type == 'check-boxes') ? 'checkbox' : 'radio';
            this_.find('input[type=' + input_type + ']').each(function( index )
            {
              options.push($(this).val());
            });
            break;

            case 'image':
            case 'youtube':
              var input_type = (type == 'image') ? 'img' : 'iframe';
              question = this_.find(input_type).attr('src');
              break;
        }

        form.push({
          type : type,
          question : question,
          options : options,
          description : this_.find('.help').html()
        });
      }
    });

    var data = {
      setting : {
        color : {
          background : {
            hex : background_color,
            is_light : tinycolor('#' + background_color).isLight()
          },
          header : {
            hex : header_color,
            is_light : tinycolor('#' + header_color).isLight()
          }
        },
        font : {
          header : $('select[name=font-header]').find(":selected").val(),
          text : $('select[name=font-text]').find(":selected").val()
        },
        button_text : {
          next : $('input[name=button-next]').val(),
          back : $('input[name=button-back]').val(),
          submit : $('input[name=button-submit]').val()
        },
        complete_text : $('textarea.complete-text').val()
      },
      form : form
    };

    var url = $(this).attr('dt-url');
    $.post(url , data ,
      function(result)
      {
        alert(JSON.stringify(result));
      }
    );
	});
});

function check_font()
{
  var header = $('select[name=font-header]').find(":selected").val();
  var text = $('select[name=font-text]').find(":selected").val();

  $('.container.form-container.forms .columns.header').css('font-family' , header);
  $('.container.form-container.forms .columns:not(.header)').css('font-family' , text);
}

function check_button()
{
  var next = $('input[name=button-next]').val();
  var back = $('input[name=button-back]').val();
  var submit = $('input[name=button-submit]').val();

  $('.button-next-text').html(next);
  $('.button-back-text').html(back);
  $('.button-submit-text').html(submit);
}

function check_complete()
{
  var text = $('.complete-text').val();
  $('.columns.complete .complete-description').html(text);
}

function get_form()
{
  const params = new URLSearchParams(window.location.search);
  var data = {
    file : params.get('file')
  };
  var url = $('.form_list').attr('dt-url');
  $.get(url , data ,
    function(result)
    {
      $('.form_list').html(result);

      go_top();
      form_datepicker();
      check_font();
      check_button();
      check_complete();
    }
  );
}

get_form();
