var page;
var background_color = "ECECEC" , header_color = "4A4A4A";

$(document).ready(function()
{
  $(document).on('keyup', '.form-container.forms .input , .form-container.forms .textarea', function()
	{
    if($(this).val().trim().length > 0)
    {
      $(this).removeClass('is-danger');
      var field = $(this).closest('.field');
      clear_notify(field);
    }
	});

  $(document).on('click', 'input[type=radio] , input[type=checkbox]', function()
	{
    var field = $(this).closest('.field');
    clear_notify(field);
	});

  $(document).on('click', '.field .image img', function()
	{
    var image_url = $(this).attr('src');

    var this_ = $('.modal.modal-image');
    this_.find('img').attr('src' , image_url);
    this_.addClass('is-active');
	});

  $(document).on('click', '.btn-next', function()
	{
    page = parseInt($(this).closest('.columns.page').attr('dt-page'));
    var next_page = page + 1;

    open_page(next_page , true);
	});

  $(document).on('click', '.btn-back', function()
	{
    page = parseInt($(this).closest('.columns.page').attr('dt-page'));
    var before_page = page - 1;

    open_page(before_page , false);
	});

  $(document).on('click', '.btn-submit', function()
	{
    if(check_input())
    {
      $('.columns.page').each(function( index )
      {
        $(this).removeClass('open');
      });

      $('.columns.complete').addClass('open');
      go_top();
    }
	});
});

function form_datepicker()
{
  $('.date').daterangepicker({
    singleDatePicker : true,
    locale : {
        format : 'MM/DD/YYYY'
    }
  });

  $('.date-range').daterangepicker({
    autoApply : true,
    locale : {
        format : 'MM/DD/YYYY',
        separator : ' - '
    }
  });
}

function clear_notify(field)
{
  field.find('.help.is-danger').remove();
  field.find('.help.hide').removeClass('hide');
}

function check_input()
{
  var result = true;

  $('.columns.page.open .field').each(function( index )
  {
    var this_ = $(this);
    var type = this_.attr('dt-type');

    if(type)
    {
      var notify = '<p class="help is-danger">This field is required.</p>'

      var required = this_.attr('dt-required') == 'y';
      if(required)
      {
        var notified = false;
        switch(type)
        {
          case 'single-line-answer':
          case 'multiple-line-answer':
          case 'date':
          case 'date-range':
            var input_type = (type == 'multiple-line-answer') ? 'textarea' : 'input';
            var input = this_.find('.' + input_type);

            if(input.val().trim().length == 0)
            {
              input.addClass('is-danger');
              notified = true;
            }
            break;

          case 'check-boxes':
          case 'multiple-choice':
            var input_type = (type == 'check-boxes') ? 'checkbox' : 'radio';
            var checked = false;

            this_.find('input[type=' + input_type + ']').each(function( index )
            {
              if($(this).is(':checked')) checked = true;
            });

            if(!checked) notified = true;
            break;
        }

        if(notified)
        {
          this_.find('.help').addClass('hide');
          this_.append(notify);

          result = false;
        }
      }
    }
  });

  return result;
}

function open_page(to_page , check)
{
  var open = true;
  if(check) open = check_input();
  if(open)
  {
    $('.columns.page').each(function( index )
    {
      $(this).removeClass('open');
      var current_page = parseInt($(this).attr('dt-page'));
      if(to_page == current_page) $(this).addClass('open');
    });

    go_top();
  }
}
