$(document).ready(function()
{
  $(document).on('click', '.modal-close , .modal-background', function()
	{
    $(this).closest('.modal').removeClass('is-active');
	});
});

function url(route)
{
  var getUrl = window.location;
  var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];

  return baseUrl + '/' + route;
}

function go_top()
{
  $('body').animate({
    scrollTop : 0
  }, 'slow');
}
