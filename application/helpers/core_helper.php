<?php
if ( !function_exists('echoPre') )
{
	function echoPre($ret)
	{
		echo "<pre>";
		print_r($ret);
		echo "</pre>";
	}
}

if ( !function_exists('createDir') )
{
	function createDir($directory , $permission = 0755)
	{
		if (!file_exists($directory))
		{
			mkdir($directory , $permission , TRUE);
		}
	}
}

if ( !function_exists('toArray') )
{
	function toArray($object)
	{
		return json_decode(json_encode($object), true);
	}
}

if ( !function_exists('val') )
{
	function val($row , $key , $default = '')
	{
		if ( is_object($row) ) $row = toArray($row);

		if ( strpos($key, '.') )
		{
			$arr = explode('.', $key);
			for( $i=0; $i<count($arr)-1;$i++ )
			{
				$row = val($row, $arr[$i], $default);
			}

			return val($row, end($arr), $default);
		}
		else
		{
			return isset($row[$key]) && $row[$key] ? $row[$key] : $default;
		}
	}
}

if ( !function_exists('url') )
{
	function url ( $url = '' )
	{
		if(isset($_SERVER['HTTPS']))
		{
			$protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
		}
		else
		{
			$protocol = 'http';
		}

		return $protocol . "://" . 'localhost/sheet2form/' . $url;
		#return $protocol . "://" . 'kost.online/' . $url;
	}
}

if ( !function_exists('isValidURL') )
{
	function isValidURL( $url )
  {
    return preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $url);
  }
}

if ( !function_exists('youtubeEmbedURL') )
{
	function youtubeEmbedURL( $url )
  {
		$shortUrlRegex = '/youtu.be\/([a-zA-Z0-9_]+)\??/i';
		$longUrlRegex = '/youtube.com\/((?:embed)|(?:watch))((?:\?v\=)|(?:\/))(\w+)/i';

		if (preg_match($longUrlRegex, $url, $matches)) {
		$youtube_id = $matches[count($matches) - 1];
		}

		if (preg_match($shortUrlRegex, $url, $matches)) {
		$youtube_id = $matches[count($matches) - 1];
		}
		return 'https://www.youtube.com/embed/' . $youtube_id ;
  }
}

if ( !function_exists('code_encode') )
{
	function code_encode( $string )
  {
		$base64 = base64_encode($string);
		$base64 = str_split($base64);
		$base64 = base64_encode(json_encode($base64));

		return $base64;
  }
}

if ( !function_exists('code_decode') )
{
	function code_decode( $string )
  {
		$json_decode = json_decode(base64_decode($string));
		$json_decode = implode($json_decode);
		$json_decode = base64_decode($json_decode);

		return $json_decode;
  }
}
?>
