<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use MatthiasMullie\Minify;
use zz\Html\HTMLMinify;

class MY_Controller extends CI_Controller {

  /**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
	}

  public function render($config , $template = 'main.html')
	{
    $global_css = ['fontawesome-free-5.5.0-web/css/all.min.css','bulma-0.7.2/css/bulma.min.css','nprogress.css','global.css'];
    $custom_css = array_merge($global_css , val($config , 'custom_css' , []));

    $global_js = ['jquery-3.3.1.min.js','nprogress.js','notify.js','global.js'];
    $custom_js = array_merge($global_js , val($config , 'custom_js' , []));

    $title = (val($config , 'title') ? val($config , 'title') . ' - ' : '') . $this->config->item('site_name');
    $generate_title = strtolower(preg_replace('/\s+/' , '' , val($config , 'title' , 'Home')));

    $data =
		[
      'title' => $title,
      'custom_css_url' => $this->_build_css($generate_title , $custom_css),
      'custom_js_url' => $this->_build_js($generate_title , $custom_js),
			'content' => val($config , 'content' , ''),
		];

    $this->_build_layout($this->load->view( 'url/'.$template , $data  , TRUE));
	}

	public function render_json($data)
	{
		header('Content-Type: application/json');
    echo json_encode($data);
	}

  private function _build_css($title , $custom)
	{
		$css_script = '';
		$minifier = new Minify\CSS();

		if ( is_array($custom) )
		{
			foreach ( $custom as $val )
			{
        if(!isValidURL($val))
				{
					$minifier->add('assets/css/'.$val);
				}
				else
				{
					$css_script .= "<link rel=\"stylesheet\" href=\"" . $val . "\" defer=\"defer\" />";
				}
			}
		}

		$generate_dir = 'assets/css/generate/' . $this->config->item('assets_version') . '/';
		createDir($generate_dir);
		$minifiedPath = $generate_dir . $title . '.css';

		$minifier->minify($minifiedPath);
		$css_script .= "<link rel=\"stylesheet\" href=\"" . $minifiedPath . "\" defer=\"defer\" />";

		return $css_script;
	}

  private function _build_js($title , $custom)
	{
		$js_script = '';
		$minifier = new Minify\JS();

		if ( is_array($custom) )
		{
			foreach ( $custom as $val )
			{
				if(!isValidURL($val))
				{
					$minifier->add('assets/js/'.$val);
				}
				else
				{
					$js_script .= '<script type="text/javascript" src="' . $val . '"></script>';
				}
			}
		}

		$generate_dir = 'assets/js/generate/' . $this->config->item('assets_version') . '/';
		createDir($generate_dir);
		$minifiedPath = $generate_dir . $title . '.js';

		$minifier->minify($minifiedPath);
		$js_script .= '<script type="text/javascript" src="' . $minifiedPath . '"></script>';

		return $js_script;
	}

  private function _build_layout( $layout )
	{
		$options = [
			'optimizationLevel' => 1,
		];
		$HTMLMinify = new HTMLMinify($layout , $options);
		echo $HTMLMinify->process();
	}

	function upload_files($uploads_dir)
	{
		$flagupload = FALSE;
		if (!empty($_FILES))
		{
			// Create upload dir
			createDir($uploads_dir);

			$tmp_name = $_FILES["file"]["tmp_name"];
			$name = $_FILES["file"]["name"];

			// Clean the fileName for security reasons
			$name = preg_replace('/[^\w\._]+/', '_', $name);
			$ext = strrpos($name , '.');

			if(file_exists($uploads_dir . DIRECTORY_SEPARATOR . $name))
			{
				$ext = strrpos($name , '.');
				$name_a = substr($name , 0 , $ext);
				$name_b = substr($name , $ext);
				$count = 1;

				while (file_exists($uploads_dir . DIRECTORY_SEPARATOR . $name_a . '_' . $count . $name_b)) $count++;
				$name = $name_a . '_' . $count . $name_b;
			}

			$file_dir = $uploads_dir."/".$name;
			If (move_uploaded_file($tmp_name , $file_dir))
			{
				$flagupload = FALSE;  //Succeed
				$file_result = str_replace('./' , '' , $file_dir);

				// Return JSON-RPC response
				die('{"jsonrpc" : "2.0" , "result" : null , "file" : "'. $file_result .'"}');
			}
			else
			{
				$flagupload = TRUE;  //Failed
			}
		}

		return $flagupload;
	}
}
