<?php
  use \Illuminate\Database\Eloquent\Model as Eloquent;

  class Forms_ extends Eloquent
  {
    protected $table = 'forms';
    public $timestamps = false;
  }
