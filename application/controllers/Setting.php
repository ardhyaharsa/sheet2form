<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Setting extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model(['Forms_']);
	}

	public function index()
	{
    $data = [];

		$config = [
			'title' => 'Setting',
			'custom_css' => ['daterangepicker.css','bulma-0.7.2/css/bulma-slider.min.css','pickr.min.css','form.css'],
			'custom_js' => ['moment.min.js','daterangepicker.min.js','bulma-slider.min.js','pickr.min.js','tinycolor.js','form.js','content/setting.js'],
			'content' => $this->_build_content()
		];

		$this->render($config);
	}

	public function form()
	{
		$file_decode = base64_decode($this->input->get('file'));
		$file = json_decode($file_decode , TRUE);

		$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file['upload']);
		$spreadsheet->setActiveSheetIndex(1);

		$sheet = $spreadsheet->getActiveSheet();
		$highestRow = $sheet->getHighestRow();
		$highestColumn = $sheet->getHighestColumn();
		$highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);

		$page = 2;
		$forms = [];
		$filter_type = ['single_line_answer','multiple_line_answer','multiple_choice','check_boxes','section_break','page_break','date','date_range','image','youtube'];
		for ($row = 5 ; $row <= $highestRow ; $row++)
		{
			$options = [];
			for ($col = 3 ; $col <= 7 ; $col++)
			{
				$value = $this->_extract_value($sheet , $col , $row);
				if(!empty($value)) $options[] = $value;
			}

			$required = $this->_extract_value($sheet , 8 , $row);

			$type = strtolower(preg_replace('/\s+/' , '_' , $this->_extract_value($sheet , 2 , $row)));
			if(in_array($type , $filter_type))
			{
				$data = [
					'question' => $this->_extract_value($sheet , 1 , $row),
          'guide' => $this->_extract_value($sheet , 9 , $row),
					'options' => $options,
					'required' => strtolower($required) == 'y' ? 'y' : 'n',
					'page' => $page
				];

				if($type == 'page_break') $page++;

				$forms[] = $this->load->view( 'box/form/'.$type.'.html' , compact('data')  , TRUE);
			}
		}

		$data = [
			'undo' => TRUE,
		];
		$forms[] = $this->load->view( 'box/form/submit.html' , compact('data')  , TRUE);

		$data = [
			'forms' => $forms,
		];
		echo $this->load->view( 'box/form/main.html' , compact('data')  , TRUE);
	}

	function save()
	{
		$post = $this->input->post();

		$data = [
			'setting' => json_encode(val($post , 'setting')),
			'form' => json_encode(val($post , 'form')),
			'create_user_id' => 0,
			'create_date' => date('Y-m-d H:i:s')
		];
		Forms_::insert($data);

		$return = [
			'result' => TRUE
		];
		$this->render_json($return);
	}

  private function _build_content()
  {
		$font_family = [
			'unset' => 'Normal',
			'cursive' => 'Cursive',
			'fantasy' => 'Fantasy',
			'monospace' => 'Monoscape',
			'sans-serif' => 'Sans Serif',
			'serif' => 'Serif'
		];

		$modal = [
			'image' => $this->load->view( 'box/form/modal_image.html' , []  , TRUE)
		];

    $dt = [
			'font_family' => $font_family,
			'modal' => $modal
		];

		return $this->load->view( 'content/setting.html' , $dt  , TRUE);
  }

	private function _extract_value($sheet , $col , $row)
	{
		return trim($sheet->getCellByColumnAndRow($col , $row)->getValue());
	}
}
