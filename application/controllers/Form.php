<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model(['Forms_']);
	}

	public function index($formID)
	{
    /*
    $where = [
      'id' => code_decode($formID),
    ];
    $form = Forms_::where($where)->first();

    $forms = [
      'setting' => json_decode(val($form , 'setting')),
      'form' => json_decode(val($form , 'form'))
    ];
    */

    $data = [];

		$config = [
			'title' => 'Setting',
			'custom_css' => ['daterangepicker.css','bulma-0.7.2/css/bulma-slider.min.css','pickr.min.css','content/form.css'],
			'custom_js' => ['moment.min.js','daterangepicker.min.js','bulma-slider.min.js','pickr.min.js','tinycolor.js','content/form.js'],
			'content' => $this->_build_content()
		];

		$this->render($config);
	}

  private function _build_content()
  {
		$modal = [
			'image' => $this->load->view( 'box/form/modal_image.html' , []  , TRUE)
		];

    $dt = [
			'modal' => $modal
		];

		return $this->load->view( 'content/form.html' , $dt  , TRUE);
  }
}
