<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Home extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data = [];

		$config = [
			'custom_css' => ['content/home.css'],
			'custom_js' => ['plupload.full.min.js','content/home.js'],
			'content' => $this->load->view( 'content/home.html' , $data  , TRUE)
		];

		$this->render($config);
	}

	public function download()
	{
		$spreadsheet = new Spreadsheet();
		$this->_set_header($spreadsheet);

		$exampleSheet = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, 'Example');
		$spreadsheet->addSheet($exampleSheet, 1);

		$spreadsheet->setActiveSheetIndex(1);

		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setCellValue('A1', 'This is only Example!! Please use sheet `Worksheet` to submit Form')

					->setCellValue('A6', '1914 translation by H. Rackham')
					->setCellValue('B6', 'Section Break')
					->setCellValue('I6', 'On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment.')

					->setCellValue('A7', 'What is Lorem Ipsum?')
					->setCellValue('B7', 'Single Line Answer')
					->setCellValue('H7', 'Y')

					->setCellValue('A8', 'Why do we use it?')
					->setCellValue('B8', 'Multiple Line Answer')
					->setCellValue('H8', 'Y')
					->setCellValue('I8', 'Contrary to popular belief, Lorem Ipsum is not simply random text.')

					->setCellValue('B9', 'Page Break')

					->setCellValue('A11', '1914 translation by H. Rackham')
					->setCellValue('B11', 'Section Break')
					->setCellValue('I11', 'On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment.')

					->setCellValue('A12', 'Where does it come from?')
					->setCellValue('B12', 'Multiple Choice')
					->setCellValue('C12', 'Paragraph')
					->setCellValue('D12', 'Words')
					->setCellValue('E12', 'Bytes')
					->setCellValue('F12', 'Lists')
					->setCellValue('H12', 'Y')

					->setCellValue('A13', 'Where can I get some?')
					->setCellValue('B13', 'Check Boxes')
					->setCellValue('C13', 'Chrome')
					->setCellValue('D13', 'Firefox')
					->setCellValue('E13', 'Safari')
					->setCellValue('F13', 'Opera')
					->setCellValue('I13', 'The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.')

					->setCellValue('B14', 'Page Break')

					->setCellValue('A16', 'What is Lorem Ipsum?')
					->setCellValue('B16', 'Date')
					->setCellValue('I16', 'Contrary to popular belief, Lorem Ipsum is not simply random text.')

					->setCellValue('A17', '1914 translation by H. Rackham')
					->setCellValue('B17', 'Section Break')
					->setCellValue('I17', 'On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment.')

					->setCellValue('A18', 'Where does it come from?')
					->setCellValue('B18', 'Date Range')
					->setCellValue('H18', 'Y')

					->setCellValue('B19', 'Page Break')

					->setCellValue('A21', 'https://via.placeholder.com/1920x540.png/ececec/4a4a4a')
					->setCellValue('B21', 'Image')
					->setCellValue('I21', 'On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment.')

					->setCellValue('A22', '1914 translation by H. Rackham')
					->setCellValue('B22', 'Section Break')
					->setCellValue('I22', 'On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment.')

					->setCellValue('A23', 'https://www.youtube.com/watch?v=2csQjIgwztE')
					->setCellValue('B23', 'Youtube')
					->setCellValue('I23', 'On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment.');

		$sheet->mergeCells('A1:I2');
		$sheet->getProtection()->setSheet(true);

		$styleHeader = [
	    'font' => [
					'color' => ['argb' => 'CE181E'],
	    ],
	    'alignment' => [
	        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
	    ]
		];
		$sheet->getStyle('A1:I2')->applyFromArray($styleHeader);

		$this->_set_header($spreadsheet);

		$spreadsheet->setActiveSheetIndex(0);
		$writer = new Xlsx($spreadsheet);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$this->config->item('site_name').' Template.xlsx"');
		header('Cache-Control: max-age=0');
		$writer->save('php://output');
	}

	public function upload()
	{
  	$uploads_dir = './assets/resources/sheet';
		return $this->upload_files($uploads_dir);
	}

	private function _set_header($spreadsheet)
	{
		$sheet = $spreadsheet->getActiveSheet();

		$sheet->setCellValue('A3', 'Question')
					->setCellValue('B3', 'Type')
					->setCellValue('C3', 'Options')
					->setCellValue('C4', '1')
					->setCellValue('D4', '2')
					->setCellValue('E4', '3')
					->setCellValue('F4', '4')
					->setCellValue('G4', '5')
					->setCellValue('H3', 'Required')
					->setCellValue('I3', 'Guide');

		$sheet->getColumnDimension('A')->setWidth(35);
		$sheet->getColumnDimension('B')->setWidth(25);
		$sheet->getColumnDimension('C')->setWidth(20);
		$sheet->getColumnDimension('D')->setWidth(20);
		$sheet->getColumnDimension('E')->setWidth(20);
		$sheet->getColumnDimension('F')->setWidth(20);
		$sheet->getColumnDimension('G')->setWidth(20);
		$sheet->getColumnDimension('H')->setWidth(15);
		$sheet->getColumnDimension('I')->setWidth(150);

		$sheet->mergeCells('A3:A4')
					->mergeCells('B3:B4')
					->mergeCells('C3:G3')
					->mergeCells('H3:H4')
					->mergeCells('I3:I4');

		$styleHeader = [
	    'font' => [
	        'bold' => true,
	    ],
	    'alignment' => [
	        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
	    ],
	    'fill' => [
	        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
	        'startColor' => [
	            'argb' => 'ffffd7',
	        ],
	    ],
			'borders' => [
        'allBorders' => [
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            'color' => ['argb' => '666666'],
        ],
    	]
		];
		$sheet->getStyle('A3:I4')->applyFromArray($styleHeader);
	}
}
